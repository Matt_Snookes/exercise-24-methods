﻿using System;


namespace exercise_24_methods



{
    class Program
    {
        //method that adds gst
        public static double addGST (double num_1 )
        {
            num_1=(num_1*1.15);
            return (num_1);
        }
        
        public static string multiply3Numbers (int num_1 , int num_2 , int num_3 )
        {
            return ($"{num_1*num_2*num_3}");
        }

        //method that returns a value
        public static string showNameAndAge2 (string name , int age)
        {
            return ($"My name is {name} and my age is {age}");
        }

        //type void method (does not return a value)
        public static void showNameAndAge (string name , int age)
        {
            Console.WriteLine($"My name is {name} and my age is {age}");
        }

        //method
        public static void myMethod()
        {
            Console.WriteLine("My first method");
            
        }
        //method
        public static void PrintGreeting (string name)
        {
            Console.WriteLine($"Hello {name}");
        }
        //main method
        public static void Main(string[] args)
        {
           //calling methods
            myMethod();
            myMethod();
            myMethod();
            myMethod();
            myMethod();
            //parsing in parameters to methods
            PrintGreeting("Jeff");
            PrintGreeting("John");
            PrintGreeting("Murray");
            PrintGreeting("Ray");
            PrintGreeting("Stefan");
            showNameAndAge ("Matt" , 39);


            //methods that return a value
            Console.WriteLine (showNameAndAge2("John" , 37));

            Console.WriteLine (multiply3Numbers ( 2 , 2 , 2));

            //calling the addGST method 
            Console.WriteLine (addGST(100.00));

        }
        
        
            
        

        
        
    }
}
